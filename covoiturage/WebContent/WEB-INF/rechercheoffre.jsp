<%@include file="header.jsp" %>
  
  <%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
  <%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>

<div class="container">


<div class="jumbotron">
  	<h2 class="text-center">
				  	<c:out value="${depart}"/> -> <c:out value="${arrivee}"/>
	</h2>
	<p><i>Nombre d'�l�ments trouv�s: ${fn:length(trajets)}</i></p>
</div>

		      <c:forEach items="${trajets}" var="trajet">
		      
		      
		      <div class="card border-dark row">
				  <div class="card-header">
				  <i><fmt:formatDate pattern="dd.MM.yyyy HH:mm" value="${ trajet.date }" /></i>
				  </div>
				  <div class="card-body">
				    <table class="table">
				        <tr>
					      <th scope="row">Conducteur</th>
					      <td><c:out value="${trajet.conducteur.name}"/></td>
					    </tr>
					    <tr>
					      <th scope="row">Mod�le de vehicule</th>
					      <td><c:out value="${trajet.vehicule.modele}"/></td>
					    </tr>
					    <tr>
					      <th scope="row">Gabarit de vehicule</th>
					      <td><c:out value="${trajet.vehicule.type.gabarit}"/></td>
					    </tr>
						<tr>
					      <th scope="row">Places disponibles</th>
					      <td><c:out value="${trajet.places}"/></td>
					    </tr>
				    </table>
				    <c:if test="${trajet.places > 0}" >
					    <form action="#" METHOD="POST">
					    	<input type="hidden" name="apicall" value="reserver">
					    	<input type="hidden" name="offre_id" value="<c:out value="${trajet.id}"/>">
					    	<select class="form-control" id="nbPlaces" name="nbPlaces" >
					    		<c:forEach var = "i" begin = "1" end = "${trajet.places}">
			  						<option><c:out value = "${i}"/></option>
			  					</c:forEach>
					    	</select>
					    	<button type="submit" class="btn btn-primary">R�server</button>
					    </form>
					 </c:if>
				  </div>
			 </div>
        	<br />
    		</c:forEach>
    		
   </div>
   
   <%@include file="js-scripts.jsp" %>
   
   
    		
<%@include file="footer.jsp" %>
