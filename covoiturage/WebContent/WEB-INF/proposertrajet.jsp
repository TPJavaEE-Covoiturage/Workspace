<%@include file="header.jsp" %>
  
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
   
	 <div class="container">
	 
	 <div class="jumbotron">
	 	<h2>Proposer un trajet</h2>
	</div>
	 
	<form action="#" METHOD="POST">
	
	  <input type="hidden" name="apicall" value="proposertrajet">
	  
	  <div class="form-group">
	    <label for="depart">Ville de depart</label>
	    <input type="input" class="form-control countrycomplete" id="depart" name="depart" autocomplete=off >
	  </div>
	  
	  <div class="form-group">
	    <label for="arrivee">Ville d'arriv�e</label>
	    <input type="input" class="form-control countrycomplete" id="arrivee" name="arrivee" autocomplete=off>
	  </div>
	  
	  <div class="form-group">
	  	<label for="datepicker">Date</label>
		<input type="text" class="form-control" id="datepicker" name="date"></p>
	  </div>

	  <div class="form-group">
	  	<label for="hour">Heure</label>
		<select class="form-control form-control" id="hour" name="hour">
			<c:forEach var = "i" begin = "0" end = "23">
		  		<option value="<c:out value = "${i}"/>"><c:out value = "${i}"/>:00 H</option>
		  	</c:forEach>
		</select>
	  </div>
	  
	  <div class="form-group">
	  	<label for="vehicule">Vehicule</label>
		<select class="form-control form-control" id="vehicule" name="vehicule">
			<c:forEach items="${vehicules}" var="vehicule">
		  		<option value="<c:out value = "${vehicule.id}"/>"><c:out value = "${vehicule.modele}"/></option>
		  	</c:forEach>
		</select>
	  </div>
	  
	  <div class="form-group">
		  <label for="pdispo">Places Disponibles:</label>
		  <input type="text" id="pdispo" name="pdispo" readonly>
		<div id="sliderpdispo"></div>
		</div>
	  
	  <button type="submit" class="btn btn-primary">Submit</button>
	  
	</form>
	</div>

	<%@include file="js-scripts.jsp" %>
	
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.4/i18n/datepicker.fr-FR.min.js" integrity="sha256-LfRC4/3bBAGT8y5dQmQSzKtY+5dfIkCvwyNj9UhgMP8=" crossorigin="anonymous"></script>

	<script> 
  		$( function() {
  			$.datepicker.regional['fr'] = {
  					closeText: 'Fermer',
  					prevText: '&#x3c;Pr�c',
  					nextText: 'Suiv&#x3e;',
  					currentText: 'Aujourd\'hui',
  					monthNames: ['Janvier','Fevrier','Mars','Avril','Mai','Juin',
  					'Juillet','Aout','Septembre','Octobre','Novembre','Decembre'],
  					monthNamesShort: ['Jan','Fev','Mar','Avr','Mai','Jun',
  					'Jul','Aou','Sep','Oct','Nov','Dec'],
  					dayNames: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'],
  					dayNamesShort: ['Dim','Lun','Mar','Mer','Jeu','Ven','Sam'],
  					dayNamesMin: ['Di','Lu','Ma','Me','Je','Ve','Sa'],
  					weekHeader: 'Sm',
  					dateFormat: 'dd-mm-yy',
  					firstDay: 1,
  					isRTL: false,
  					showMonthAfterYear: false,
  					yearSuffix: '',
  					minDate: 0,
  					maxDate: '+12M +0D',
  					showButtonPanel: true
  			};

  			$.datepicker.setDefaults( $.datepicker.regional[ "fr" ] );
    		$( "#datepicker" ).datepicker();
    		
    		$( "#sliderpdispo" ).slider({
  		      range: "max",
  		      min: 1,
  		      max: 9,
  		      value: 3,
  		      slide: function( event, ui ) {
  		        $( "#pdispo" ).val( ui.value );
  		      }
  		    });
    		
  		    $( "#pdispo" ).val( $( "#sliderpdispo" ).slider( "value" ) );
  		} );
  		
  		
  </script>

<%@include file="footer.jsp" %>
