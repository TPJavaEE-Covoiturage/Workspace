<%@include file="header.jsp" %>
  
  <%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
  <%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>

	<div class="container">


		<div class="jumbotron">
  			<h2 class="text-center">Mes trajets propos�s</h2>
			<p><i>Actuellement ${fn:length(trajets)} trajets propos�s.</i></p>
		</div>

		     <c:forEach items="${trajets}" var="trajet">
		      <div class="card border-dark row">
				  <div class="card-header">
				  <i><fmt:formatDate pattern="dd.MM.yyyy HH:mm" value="${ trajet.date }" /></i>
				  <c:out value="${trajet.depart.name}"/> -> <c:out value="${trajet.arrivee.name}"/>
				  </div>
				  <div class="card-body">
				    <table class="table">

				        <tr>
					      <th scope="row">Conducteur</th>
					      <td><c:out value="${trajet.conducteur.name}"/></td>
					    </tr>
					    <tr>
					      <th scope="row">Mod�le de vehicule</th>
					      <td><c:out value="${trajet.vehicule.modele}"/></td>
					    </tr>
					    <tr>
					      <th scope="row">Gabarit de vehicule</th>
					      <td><c:out value="${trajet.vehicule.type.gabarit}"/></td>
					    </tr>
						<tr>
					      <th scope="row">Places disponibles</th>
					      <td><c:out value="${trajet.places}"/></td>
					    </tr>
				    </table>
				    <c:if test="${fn:length(reservations[trajet.id]) == 0}" >
				    <i>Pas de r�servations pour le moment...</i>
				    </c:if>
				    <c:if test="${fn:length(reservations[trajet.id]) > 0}" >
				    
				    	<h5>R�servations:</h5>
				    	<div class="card-columns">
					    <c:forEach items="${reservations[trajet.id]}" var="reservation">
					    	<div class="card" style="width: 20rem;">
					    	  <c:if test="${reservation.validation}" >
								 <div class="alert alert-success" role="alert">
				  				  Reservation accept�e !
								 </div>
							  </c:if>
							  <c:if test="${!reservation.validation}" >
							  	<div class="alert alert-warning" role="alert">
			  						R�servation en attente...
								</div>
							  </c:if>
							  <div class="card-body">
							    <h4 class="card-title"><c:out value="${reservation.passager.name}"/></h4>
							    <p class="card-text">Places : <c:out value="${reservation.places}"/></p>
							    
							    <c:if test="${!reservation.validation}" >
								    <form action="#" METHOD="POST">
								    	<input type="hidden" name="apicall" value="accepterreservation">
								    	<input type="hidden" name="reservation_id" value="<c:out value="${reservation.id}"/>">
								    	<button type="submit" class="btn btn-primary">Accepter</button>
								    </form>
							    </c:if>
							  </div>
							</div>
					    	
					    </c:forEach>
					    </div>
					 </c:if>
				</div>
					
			 </div>
        	<br />
    		</c:forEach>
    		
   </div>
   
   <%@include file="js-scripts.jsp" %>
   
   
    		
<%@include file="footer.jsp" %>