<%@include file="header.jsp" %>
  
 	<!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
      <div class="container">
        <h1 class="display-3">Bienvenue !</h1>
        <p>EcoVoiturage est une plateforme de covoiturage ecoresponsable !</p>
      </div>
    </div>

<%@include file="js-scripts.jsp" %>

<%@include file="footer.jsp" %>
