<%@include file="header.jsp" %>

<div class="container">
  <div class="row">
     <div class="col-12">
     <h2 style="margin:20px;">G�rer les types de voitures</h2>
     </div>
     <div class="col-12">
		<table class="table table-dark">
		  <thead>
		    <tr>
		      <th scope="col-8">Type de voiture</th>
		      <th scope="col-4">Action</th>
		    </tr>
		  </thead>
		  <tbody>
		  
		  		<form METHOD="POST" target="#">
		  		<input type="hidden" name="apicall" value="addtype">
			  		<tr>
			  			<td><input name="typename">
			  			</td>
			  			<td> <button type="submit" class="btn btn-primary">Ajouter</button> </td>
			  		</tr>
		  		</form>
		  
		      <c:forEach items="${types}" var="type">
        		<tr>
            		<td><c:out value="${type.gabarit}"/></td>
            		<td>
            		 
            		<form METHOD="POST" target="#">
            			<input type="hidden" name="apicall" value="deletetype">
            			<input type="hidden" name="typename" value="<c:out value="${type.gabarit}"/>">
  						<button type="submit" class="btn btn-primary">Supprimer</button>
            		</form>
            		
            		</td>  
        		</tr>
    		</c:forEach>
		</table>
	</div>
  </div>
</div>
<%@include file="js-scripts.jsp" %>



<%@include file="footer.jsp" %>
