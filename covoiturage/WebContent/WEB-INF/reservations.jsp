<%@include file="header.jsp" %>
    	<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
  		<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
  
		<div class="container">
		
		
		<div class="jumbotron">
			<h2 class="text-center">Mes r�servations</h2>
			<p><i>Nombre de reservations: ${fn:length(reservations)}</i></p>
		</div>

		      <c:forEach items="${reservations}" var="reservation">
		      
		      
		      <div class="card border-dark row">
				  <div class="card-header">
				  	<i><fmt:formatDate pattern="dd.MM.yyyy HH:mm" value="${ reservation.offre.date }" /></i>
				  	<c:out value="${reservation.offre.depart.name}"/> -> <c:out value="${reservation.offre.arrivee.name}"/>
				  </div>
				  <div class="card-body">
				  
				  <c:if test="${!reservation.validation}" >
				  	<div class="alert alert-warning" role="alert">
  						En Attente de validation...
					</div>
				  </c:if>
				  
				  <c:if test="${reservation.validation}" >
				  	<div class="alert alert-success" role="alert">
  						Reservation Valid�e !
					</div>
				  </c:if>
				  
				    <table class="table">
				        <tr>
					      <th scope="row">Conducteur</th>
					      <td><c:out value="${reservation.offre.conducteur.name}"/></td>
					    </tr>
					    <tr>
					      <th scope="row">Mod�le de vehicule</th>
					      <td><c:out value="${reservation.offre.vehicule.modele}"/></td>
					    </tr>
					    <tr>
					      <th scope="row">Gabarit de vehicule</th>
					      <td><c:out value="${reservation.offre.vehicule.type.gabarit}"/></td>
					    </tr>
						<tr>
					      <th scope="row">Nombre de places demand�es</th>
					      <td><c:out value="${reservation.places}"/></td>
					    </tr>
				    </table>
				  </div>
			 </div>
        	<br />
    		</c:forEach>
    		
   </div>
   
<%@include file="js-scripts.jsp" %>

<%@include file="footer.jsp" %>