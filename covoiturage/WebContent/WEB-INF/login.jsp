<%@include file="header.jsp" %>
  

  <div class="container">
  
  	 <div class="jumbotron">
	 	<h1 class="text-center">Connexion / Enregistrement</h1>
	</div>
  
  
  <div class="row">
	<div class="col-6">
      <form class="form-signin" METHOD="POST">
        <input type="hidden" name="apicall" value="login">
        <label for="name" class="sr-only">Nom</label>
        <input type="text" id="name" name="name" class="form-control" placeholder="Nom" autocomplete=off required>
        <label for="password" class="sr-only">Mot de Passe</label>
        <input type="password" id="password" name="password" class="form-control" placeholder="Mot de Passe" autocomplete=off required>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Connexion</button>
      </form>
     </div>
      
     <div class="col-6">
      <form class="form-signin" METHOD="POST">
        <input type="hidden" name="apicall" value="sub">
        <label for="name" class="sr-only">Nom</label>
        <input type="text" id="name" name="name" class="form-control" placeholder="Nom" autocomplete=off required>
        <label for="password" class="sr-only">Mot de Passe</label>
        <input type="password" id="password" name="password" class="form-control" placeholder="Mot de Passe" autocomplete=off required>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Enregistrement</button>
      </form>
     </div>
     </div>
    </div> <!-- /container -->

<%@include file="js-scripts.jsp" %>

<%@include file="footer.jsp" %>
