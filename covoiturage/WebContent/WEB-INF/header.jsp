<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
    <title>EcoVoiturage</title>
    
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous" />
 
</head>
  <body style="padding-top: 3.5rem;">
  <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
      <a class="navbar-brand" href="?viewcall=">EcoVoiturage</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
        
        <c:if test="${!isLogged}" >
	          <li class="nav-item">
	            <a class="nav-link" href="?viewcall=login">Se connecter</a>
	          </li>
          </c:if>
          
          <c:if test="${isLogged}" >
	          <li class="nav-item dropdown">
	            <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Mon compte</a>
	            <div class="dropdown-menu" aria-labelledby="dropdown01">
	              <a class="dropdown-item" href="?viewcall=trajets">Mes trajets propos�s</a>
	              <a class="dropdown-item" href="?viewcall=reservations">Mes r�servations</a>
	              <a class="dropdown-item" href="?viewcall=vehicules">Mes v�hicules</a>
	              <a class="dropdown-item" href="?viewcall=proposertrajet">Proposer un trajet</a>
	              <a class="dropdown-item" href="?apicall=logout">Se d�connecter</a>
	            </div>
	          </li>
          </c:if>

          <c:if test="${isAdmin}" >
	          <li class="nav-item dropdown">
	            <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Administration</a>
	            <div class="dropdown-menu" aria-labelledby="dropdown01">
	              <a class="dropdown-item" href="?viewcall=gerervilles">G�rer les villes</a>
	              <a class="dropdown-item" href="?viewcall=gerertypesvoitures">G�rer les types de voitures</a>
	            </div>
	          </li>
          </c:if>
        </ul>
        
        <form class="form-inline my-2 my-lg-0" method="GET" >
          <input type="hidden" name="viewcall" value="cherchertrajet">
          <input autocomplete=off name="Depart" class="countrycomplete form-control mr-sm-2" type="text" placeholder="D�part" aria-label="Depart" />
          <input autocomplete=off name="Arrivee" class="countrycomplete form-control mr-sm-2" type="text" placeholder="Arriv�e" aria-label="Arriv�e" />
          <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Cherchez un trajet</button>
        </form>
      </div>
    </nav>
