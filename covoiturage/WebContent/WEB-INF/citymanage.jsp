<%@include file="header.jsp" %>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="container">
  <div class="row">
     <div class="col-12">
     <h2 style="margin:20px;">G�rer les villes</h2>
     </div>
     <div class="col-12">
		<table class="table table-dark">
		  <thead>
		    <tr>
		      <th scope="col-8">Nom de la ville</th>
		      <th scope="col-4">Action</th>
		    </tr>
		  </thead>
		  <tbody>
		  
		  		<form METHOD="POST" target="#">
		  		<input type="hidden" name="apicall" value="addcity">
			  		<tr>
			  			<td><input name="cityname">
			  			</td>
			  			<td> <button type="submit" class="btn btn-primary">Ajouter</button> </td>
			  		</tr>
		  		</form>
		  
		      <c:forEach items="${villes}" var="ville">
        		<tr>
            		<td><c:out value="${ville.name}"/></td>
            		<td>
            		 
            		<form METHOD="POST" target="#">
            			<input type="hidden" name="apicall" value="deletecity">
            			<input type="hidden" name="cityname" value="<c:out value="${ville.name}"/>">
  						<button type="submit" class="btn btn-primary">Supprimer</button>
            		</form>
            		
            		</td>  
        		</tr>
    		</c:forEach>
		</table>
	</div>
  </div>
</div>

<%@include file="js-scripts.jsp" %>

<%@include file="footer.jsp" %>
