<%@include file="header.jsp" %>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="container">

	 <div class="jumbotron">
	 	<h2>G�rer vos v�hicules</h2>
	</div>
	 
  <div class="row">
     <div class="col-12">
		<table class="table">
		  <thead>
		    <tr>
		      <th scope="col-4">Nom du mod�le</th>
		      <th scope="col-4">Gabarit</th>
		      <th scope="col-4">Action</th>
		    </tr>
		  </thead>
		  <tbody>
		  
		  		<form METHOD="POST" target="#">
		  		<input type="hidden" name="apicall" value="addvehicule">
			  		<tr>
			  			<td><input name="modele"></td>
			  			<td>
			  				<select class="form-control form-control" id="gabarit" name="gabarit">
			  					<c:forEach items="${types}" var="type">
			  						<option><c:out value="${type.gabarit}"/></option>
			  					</c:forEach>
			  				</select>
			  			</td>
			  			<td> <button type="submit" class="btn btn-primary">Ajouter</button> </td>
			  		</tr>
		  		</form>
		  
		      <c:forEach items="${vehicules}" var="vehicule">
        		<tr>
            		<td><c:out value="${vehicule.modele}"/></td>
            		<td><c:out value="${vehicule.type.gabarit}"/></td>
            		<td>
            		 
            		<form METHOD="POST" target="#">
            			<input type="hidden" name="apicall" value="deletevehicule">
            			<input type="hidden" name="vehiculeid" value="<c:out value="${vehicule.id}"/>">
  						<button type="submit" class="btn btn-primary">Supprimer</button>
            		</form>
            		
            		</td>  
        		</tr>
    		</c:forEach>
		</table>
	</div>
  </div>
</div>

<%@include file="js-scripts.jsp" %>

<%@include file="footer.jsp" %>
