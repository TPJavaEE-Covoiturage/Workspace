package servlets;


import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Entity.Offre;
import Entity.Reservation;
import Entity.TypeVehicule;
import Entity.Vehicule;
import Entity.Ville;
import ejbs.OffreDAO;
import ejbs.ReservationsDAO;
import ejbs.TypeVoituresDAO;
import ejbs.UsersDAO;
import ejbs.VehiculeDAO;
import ejbs.VillesDAO;

@WebServlet("/covoiturage")
public class CovoiturageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB(name="VillesDAO")
	private VillesDAO villesDAO;

	@EJB(name="TypeVoituresDAO")
	private TypeVoituresDAO typevoituresDAO;
	
	@EJB(name="OffreDAO")
	private OffreDAO offreDAO;
	
	@EJB(name="UsersDAO")
	private UsersDAO usersDAO;
	
	@EJB(name="VehiculeDAO")
	private VehiculeDAO vehiculeDAO;
	
	@EJB(name="ReservationsDAO")
	private ReservationsDAO reservationsDAO;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		
		//Api call for database actions
		String apicall = request.getParameter("apicall");
		if (apicall != null && apicall != ""){
			if (api.APICall(
					request,
					response,
					villesDAO,
					typevoituresDAO,
					usersDAO,
					vehiculeDAO, 
					offreDAO, 
					reservationsDAO,
					apicall,
					session
				) == 0) {
				return;
			}
		}
		
		//Attributes of logged and admin
		request.setAttribute("isLogged", api.isLogged(session));
		request.setAttribute("isAdmin", api.isAdmin(session, usersDAO));

		//View call for different views
		String viewcall = request.getParameter("viewcall");
		if (viewcall != null && viewcall != ""){
			switch (viewcall) {
			case "vehicules":
				if (api.isLogged(session)) {
					try {
						List<Vehicule> vehicules = vehiculeDAO.listForUser((String) session.getAttribute("username"));
						List<TypeVehicule> typesvehicules = typevoituresDAO.list();
						request.setAttribute("vehicules", vehicules);
						request.setAttribute("types", typesvehicules);
						this.getServletContext().getRequestDispatcher("/WEB-INF/gerervehicules.jsp").forward(request, response);
						return;
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				break;
			
			case "gerervilles":
				try {
					List<Ville> villes = villesDAO.list();
					request.setAttribute("villes", villes);
					this.getServletContext().getRequestDispatcher("/WEB-INF/citymanage.jsp").forward(request, response);
					return;

				} catch (Exception e) {
					e.printStackTrace();
				}
				break;
		
			case "gerertypesvoitures":
				try {
					List<TypeVehicule> types = typevoituresDAO.list();
					request.setAttribute("types", types);
					this.getServletContext().getRequestDispatcher("/WEB-INF/typevoituresmanage.jsp").forward(request, response);
					return;

				} catch (Exception e) {
					e.printStackTrace();
				}
				break;

				
			case "cherchertrajet":
				String depart = request.getParameter("Depart");
				String arrivee = request.getParameter("Arrivee");
				if (depart != null && depart != "" && arrivee != null && arrivee != ""){
					List<Offre> trajets;
					try {
						trajets = offreDAO.search(depart, arrivee);
						request.setAttribute("depart", depart);
						request.setAttribute("arrivee", arrivee);
						request.setAttribute("trajets", trajets);
						this.getServletContext().getRequestDispatcher("/WEB-INF/rechercheoffre.jsp").forward(request, response);
						return;
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				break;

			case "trajets":
				if (api.isLogged(session)) {
					List<Offre> trajets;
					try {
						trajets = offreDAO.listePourConducteur((String) session.getAttribute("username"));
						
						Map<Integer, List<Reservation>> reservations = new HashMap<>();
						for (Offre trajet : trajets) {
							reservations.put(
									trajet.getId(),
									reservationsDAO.listeReservationsPourOffre(trajet.getId())
							);
						}
						
						request.setAttribute("trajets", trajets);
						request.setAttribute("reservations", reservations);
						this.getServletContext().getRequestDispatcher("/WEB-INF/trajets.jsp").forward(request, response);
						return;
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				break;

			case "reservations":
				if (api.isLogged(session)) {
					List<Reservation> reservations;
					try {
						reservations = reservationsDAO.listeReservationsPour((String) session.getAttribute("username"));
						request.setAttribute("reservations", reservations);
						this.getServletContext().getRequestDispatcher("/WEB-INF/reservations.jsp").forward(request, response);
						return;
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				break;
			case "proposertrajet":
				if (api.isLogged(session)) {
					try {
						List<Vehicule> vehicules = vehiculeDAO.listForUser((String) session.getAttribute("username"));
						System.out.println(vehicules);
						request.setAttribute("vehicules", vehicules);
						this.getServletContext().getRequestDispatcher("/WEB-INF/proposertrajet.jsp").forward(request, response);
						return;
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				break;

			case "login":
				if (!api.isLogged(session)) {
					this.getServletContext().getRequestDispatcher("/WEB-INF/login.jsp").forward(request, response);
					return;
				}
				break;

			default:	
				break;

			}
		}
		
		//Default view
		this.getServletContext().getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
