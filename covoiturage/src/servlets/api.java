package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Entity.TypeVehicule;
import Entity.Ville;
import ejbs.OffreDAO;
import ejbs.ReservationsDAO;
import ejbs.TypeVoituresDAO;
import ejbs.UsersDAO;
import ejbs.VehiculeDAO;
import ejbs.VillesDAO;

public class api {
	
	public static Boolean isLogged(HttpSession session) {
		return session.getAttribute("username") != null;
	}
	
	public static Boolean isAdmin(HttpSession session,UsersDAO usersDAO) {
		String user = (String) session.getAttribute("username");
		return user != null && usersDAO.IsAdmin(user);
	}
	
	
	public static int APICall(HttpServletRequest request,
			HttpServletResponse response,
			VillesDAO villesDAO,
			TypeVoituresDAO typevoituresDAO,
			UsersDAO usersDAO,
			VehiculeDAO vehiculeDAO, 
			OffreDAO offreDAO,
			ReservationsDAO reservationsDAO,
			String apicall, 
			HttpSession session) {
		
		switch (apicall) {
		
		/*
		 * Reservation
		 */
		case "reserver":
			if (api.isLogged(session)) {
				String offre_id = request.getParameter("offre_id");
				String nb_places = request.getParameter("nbPlaces");
				
				try {
					int int_offre_id = Integer.parseInt(offre_id);
					int int_nb_places = Integer.parseInt(nb_places);
					
					reservationsDAO.reserver(int_nb_places, int_offre_id, (String) session.getAttribute("username"));
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return 1;
			
		case "accepterreservation":
			if (api.isLogged(session)) {
				String reservation_id = request.getParameter("reservation_id");
				
				try {
					int int_reservation_id = Integer.parseInt(reservation_id);
					
					reservationsDAO.accepter(int_reservation_id, (String) session.getAttribute("username"));
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return 1;
			
		/*
		 * Appels de gestion des offres
		 */
		case "proposertrajet":
			if (api.isLogged(session)) {
				String depart = request.getParameter("depart");
				String arrivee = request.getParameter("arrivee");
				String date = request.getParameter("date");
				String hour = request.getParameter("hour");
				String vehicule = request.getParameter("vehicule");
				String pdispo = request.getParameter("pdispo");
				
				try {
					int int_hour = Integer.parseInt(hour);
					int int_pdispo = Integer.parseInt(pdispo);
					int int_vehicule = Integer.parseInt(vehicule);
					//Parse Date
					SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
					Date parsedDate = dateFormat.parse(date);
					Calendar calendar = Calendar.getInstance();
			        calendar.setTime(parsedDate);
			        calendar.set(Calendar.MILLISECOND, 0);
			        calendar.set(Calendar.SECOND, 0);
			        calendar.set(Calendar.MINUTE, 0);
			        calendar.set(Calendar.HOUR, int_hour);
					parsedDate = calendar.getTime();
					
					Timestamp timestamp = new Timestamp(parsedDate.getTime());
					
					offreDAO.proposerTrajet(
							depart,arrivee,
							int_vehicule,
							int_pdispo,
							timestamp,
							(String) session.getAttribute("username")
					);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return 1;
			
		/*
		 * Appels des villes
		 * 
		 */
		case "listville":
		    response.setContentType("application/json");
		    response.setCharacterEncoding( "UTF-8" );
		    try {
				PrintWriter out = response.getWriter();
				out.print("[");
				try {
					List<Ville> villes = villesDAO.list();
					ArrayList<String> villesnames =  new ArrayList<String>();
					for (Ville ville : villes) {
						villesnames.add('"'+ville.getName()+'"');
					}
					out.print(String.join(",",villesnames));
				} catch (Exception e) {
					e.printStackTrace();
				}
				out.print("]");
			} catch (IOException e) {
				e.printStackTrace();
			}
			return 0;
			
		case "addcity":
			if (api.isAdmin(session, usersDAO)) {
				String cityname = request.getParameter("cityname");
				Ville ville = new Ville();
				ville.setName(cityname);
				try {
					villesDAO.add(ville);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return 1;

		case "deletecity":
			if (api.isAdmin(session, usersDAO)) {
				String cityname1 = request.getParameter("cityname");
				Ville ville1 = new Ville();
				ville1.setName(cityname1);
				try {
					villesDAO.remove(ville1);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return 1;

		/*
		 * Gestion des vehicules
		 * 
		 */
		case "addvehicule":
			if (api.isLogged(session)) {
				String modele = request.getParameter("modele");
				String gabarit = request.getParameter("gabarit");
				String user = (String) session.getAttribute("username");
				vehiculeDAO.add(modele,gabarit,user);
			}
			return 1;

		case "deletevehicule":
			if (api.isLogged(session)) {
				String id_string = request.getParameter("vehiculeid");
				try{
					int id = Integer.parseInt(id_string);
					vehiculeDAO.delete(id,(String) session.getAttribute("username"));
				}catch(Exception e){
			        e.printStackTrace();
			    }
			}
			return 1;
			
		/*
		 * Appels des types de vehicules
		 * 
		 */
		case "listtypes":
		    response.setContentType("application/json");
		    response.setCharacterEncoding( "UTF-8" );
		    try {
				PrintWriter out = response.getWriter();
				out.print("[");
				try {
					List<TypeVehicule> types = typevoituresDAO.list();
					ArrayList<String> typesnames =  new ArrayList<String>();
					for (TypeVehicule type : types) {
						typesnames.add('"'+type.getGabarit()+'"');
					}
					out.print(String.join(",",typesnames));
				} catch (Exception e) {
					out.print("\"An error occured\"");
					e.printStackTrace();
				}
				out.print("]");
			} catch (IOException e) {
				e.printStackTrace();
			}
			return 0;
			
		case "addtype":
			if (api.isAdmin(session, usersDAO)) {
				String typename = request.getParameter("typename");
				TypeVehicule type = new TypeVehicule();
				type.setGabarit(typename);
				try {
					typevoituresDAO.add(type);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return 1;
			
		case "deletetype":
			if (api.isAdmin(session, usersDAO)) {
				String typename1 = request.getParameter("typename");
				TypeVehicule type1 = new TypeVehicule();
				type1.setGabarit(typename1);
				try {
					typevoituresDAO.remove(type1);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			return 1;
			
			
			
		/*
		 * Appels de gestion de sessions utilisateurs
		 * 
		 */
		case "login":
			String username = request.getParameter("name");
			String password = request.getParameter("password");
			if (username != null && username != "" && password != null && password != ""){
				username = usersDAO.login(password, username);
				if (username != null) {
					session.setAttribute("username", username);
				}
			}

			return 1;
			
		case "logout":
			session.setAttribute("username", null);
			return 1;
			
		case "sub":
			String username1 = request.getParameter("name");
			String password1 = request.getParameter("password");
			if (username1 != null && username1 != "" && password1 != null && password1 != ""){
				if(usersDAO.create(password1, username1, false)) {
					session.setAttribute("username", username1);
				} else {
					System.out.println("Creation Failed");
				}
			}
			return 1;
			
		/*
		 * Default
		 */	
			
		default:
			return -1;

		}
	}
}
