package Entity;


import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class User {

	@Id
	private String Name;

	private String Password;

	private Boolean IsAdmin;
	
	public String getPassword() {
		return Password;
	}
	public void setPassword(String password) {
		Password = password;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public Boolean getIsAdmin() {
		return IsAdmin;
	}
	public void setIsAdmin(Boolean isAdmin) {
		IsAdmin = isAdmin;
	}
}

