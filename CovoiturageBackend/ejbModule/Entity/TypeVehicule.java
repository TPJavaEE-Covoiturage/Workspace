package Entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class TypeVehicule {
	@Id
	private String Gabarit;

	public String getGabarit() {
		return Gabarit;
	}

	public void setGabarit(String gabarit) {
		Gabarit = gabarit;
	}
}
