package Entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
@Entity
public class Reservation {
	
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	private int Id;
	
	@ManyToOne
	private Offre Offre;
	
	@ManyToOne
	private User Passager;
	
	private int Places;
	
	private Boolean Validation;

	public User getPassager() {
		return Passager;
	}

	public void setPassager(User passager) {
		Passager = passager;
	}

	public Offre getOffre() {
		return Offre;
	}

	public void setOffre(Offre offre) {
		Offre = offre;
	}

	public Boolean getValidation() {
		return Validation;
	}

	public void setValidation(Boolean validation) {
		Validation = validation;
	}

	public int getPlaces() {
		return Places;
	}

	public void setPlaces(int nombrePlaces) {
		Places = nombrePlaces;
	}

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

}
