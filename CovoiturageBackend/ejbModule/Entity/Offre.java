package Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.util.Date;

@Entity
public class Offre {
	
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	private int Id;
	
	@Column(name = "DATETIME_FIELD")
	private java.sql.Timestamp Date; 

	@ManyToOne
	private User Conducteur;
	
	@ManyToOne
	private Vehicule Vehicule;
	
	@ManyToOne
	private Ville Depart;
	
	@ManyToOne
	private Ville Arrivee;

	private int Places;
	
	public Ville getDepart() {
		return Depart;
	}

	public void setDepart(Ville depart) {
		Depart = depart;
	}

	public Ville getArrivee() {
		return Arrivee;
	}

	public void setArrivee(Ville arrivee) {
		Arrivee = arrivee;
	}

	public Vehicule getVehicule() {
		return Vehicule;
	}

	public void setVehicule(Vehicule vehicule) {
		Vehicule = vehicule;
	}

	public User getConducteur() {
		return Conducteur;
	}

	public void setConducteur(User conducteur) {
		Conducteur = conducteur;
	}

	public java.sql.Timestamp getDate() {
		return Date;
	}

	public void setDate(java.sql.Timestamp date) {
		Date = date;
	}

	public int getPlaces() {
		return Places;
	}

	public void setPlaces(int placesDisponibles) {
		Places = placesDisponibles;
	}

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

}
