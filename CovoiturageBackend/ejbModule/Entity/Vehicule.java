package Entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
@Entity
public class Vehicule {
	
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id 
	private int Id;
	
	private String Modele;
	
	@ManyToOne
	private User Owner;
	
	@ManyToOne
	private TypeVehicule Type;

	public String getModele() {
		return Modele;
	}

	public void setModele(String modèle) {
		Modele = modèle;
	}

	public User getOwner() {
		return Owner;
	}

	public void setOwner(User user) {
		Owner = user;
	}

	public TypeVehicule getType() {
		return Type;
	}

	public void setType(TypeVehicule type) {
		Type = type;
	}

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}
	
}
