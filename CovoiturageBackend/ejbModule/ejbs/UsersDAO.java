package ejbs;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import Entity.User;

@Stateless(name="UsersDAO")
@LocalBean
public class UsersDAO {
	private static String security_salt = "fWR49KRlZgNSJdCuq0qxfo5lp2B1CVomdiQiODxypYulT9S5OmtPoAWCjtiC";
	
    @PersistenceContext( unitName = "Covoiturage" )
    private EntityManager       em;

	
	private String hash_pass(String password) {
		MessageDigest sha256;
		try {
			sha256 = MessageDigest.getInstance("SHA-256");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}
		
		//Create hash with salt
        String passWithSalt = security_salt + password;
        byte[] passBytes = passWithSalt.getBytes();
        byte[] shaDigest = sha256.digest(passBytes);
        
        //Byte to string
        String hash = "";
        for(byte b:shaDigest) {
            hash += String.format("%02x",b);
        }
        
        return hash;
	}
	
	public String login(String password,String username) { 
		User user = em.find(User.class, username);
		
		//Hash the password to avoid time based attacks
		String hashed_password = hash_pass(password);

		//Check if user exist and password
		if (user != null && hashed_password.equals(user.getPassword())) {
			return user.getName();
		} 
		
		//Return null by default
		return null;
	}
	
	public Boolean create(String password,String username,Boolean IsAdmin) { 
		
		User user = em.find(User.class, username);
		
		//User already exists
		if (user != null) {
			return false;
		}
		
		String hashed_password = hash_pass(password);
		
		//New user and persist
		User usr = new User();
		usr.setPassword(hashed_password);
		usr.setName(username);
		usr.setIsAdmin(IsAdmin);
		em.persist(usr);
		
		//Success
		return true;
	}
	
	
	public Boolean IsAdmin(String username) {
		User user = em.find(User.class, username);
		return user.getIsAdmin();
	}

}

