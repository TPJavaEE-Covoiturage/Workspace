package ejbs;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import Entity.TypeVehicule;
import Entity.User;
import Entity.Vehicule;

@Stateless(name="VehiculeDAO")
@LocalBean
public class VehiculeDAO {

    @PersistenceContext( unitName = "Covoiturage" )
    private EntityManager       em;
	
    public List<Vehicule> list() throws Exception {
        try {
            TypedQuery<Vehicule> q = em.createQuery("from Vehicule", Vehicule.class);
            return q.getResultList();
        } catch ( Exception e ) {
            throw new Exception( e );
        }
    }
    
    public List<Vehicule> listForUser(String Username) throws Exception {
        try {
            TypedQuery<Vehicule> q = em.createQuery(
            		"from Vehicule vehicule "
            		+ "left join fetch vehicule.Owner as owner "
            		+ "WHERE owner.Name = ?1", Vehicule.class);
            q.setParameter(1, Username);
            return q.getResultList();
        } catch ( Exception e ) {
            throw new Exception( e );
        }
    }

	public void remove(Vehicule vehicule) {
		em.remove(em.contains(vehicule) ? vehicule : em.merge(vehicule));
	}

	public void add(String modele, String gabarit, String username) {
		
		//Retreive user
		User user = em.find(User.class, username);
		if (user == null){ return;}
		
		//Retreive vehicule type
		TypeVehicule type = em.find(TypeVehicule.class, gabarit);
		if (type == null){ return;}
		
		//Create vehicule
		Vehicule vehicule = new Vehicule();
		vehicule.setModele(modele);
		vehicule.setOwner(user);
		vehicule.setType(type);
        em.persist(vehicule);
		
	}

	public void delete(int id, String username) {
		//Get vehicule
		Vehicule vehicule = em.find(Vehicule.class, id);
		
		System.out.println(vehicule.getOwner().getName());
		System.out.println(username);
		
		//If the user is the owner of the vehicule
		if (vehicule.getOwner().getName().equals(username)) {
			System.out.println("Remove");
			em.remove(em.contains(vehicule) ? vehicule : em.merge(vehicule));
		}
	}
}
