package ejbs;

import java.sql.Timestamp;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import Entity.Offre;
import Entity.Reservation;
import Entity.User;

@Stateless(name="ReservationsDAO")
@LocalBean
public class ReservationsDAO {

    @PersistenceContext( unitName = "Covoiturage" )
    private EntityManager       em;
	
    public void reserver(int places,int offre_id,String username) {
		//Retreive user
		User user = em.find(User.class, username);
		if (user == null){ return;}
    	
		//Retreive offre
		Offre offre = em.find(Offre.class, offre_id);
		if (offre == null){ return;}
		
		//On regarde si il reste assez de places
		if (places > offre.getPlaces()) { return; }
		
		Reservation reservation = new Reservation();
		reservation.setPlaces(places);
		reservation.setOffre(offre);
		reservation.setPassager(user);
		reservation.setValidation(false);
		em.persist(reservation);
    }
    
    public List<Reservation> listePourPassager(String username) throws Exception {
        try {
            TypedQuery<Reservation> q = em.createQuery(
            		"from Reservation r "
            		+ "left join fetch r.Passager p "
            		+ "WHERE p.Name = ?1", Reservation.class);
            q.setParameter(1, username);
            return q.getResultList();
        } catch ( Exception e ) {
            throw new Exception( e );
        }
    }

	public List<Reservation> listeReservationsPourOffre(int offre_id) throws Exception {
        try {
            TypedQuery<Reservation> q = em.createQuery(
            		"from Reservation r "
            		+ "left join fetch r.Offre o "
            		+ "WHERE o.Id = ?1", Reservation.class);
            q.setParameter(1, offre_id);
            return q.getResultList();
        } catch ( Exception e ) {
            throw new Exception( e );
        }
	}  

	public List<Reservation> listeReservationsPour(String username) throws Exception {
        try {
            TypedQuery<Reservation> q = em.createQuery("from Reservation r "
            		+ "left join fetch r.Passager p "
            		+ "WHERE p.Name = ?1", Reservation.class);
            q.setParameter(1, username);
            return q.getResultList();
        } catch ( Exception e ) {
            throw new Exception( e );
        }
	}

	public void accepter(int int_reservation_id, String username) {
		//Retreive user
		User user = em.find(User.class, username);
		if (user == null){ return;}
		
		//Retreive reservation
		Reservation reservation = em.find(Reservation.class, int_reservation_id);
		if (reservation == null){ return;}
		
		//Check if the good user accepts reservation
		if (!reservation.getOffre().getConducteur().getName().equals(user.getName())) { return;}
		
		//Places restantes
		if (reservation.getPlaces() > reservation.getOffre().getPlaces()) { return;}
		
		//Gerer places restantes
		Offre offre = reservation.getOffre();
		offre.setPlaces(offre.getPlaces()-reservation.getPlaces());
		
		//Validate
		reservation.setValidation(true);
		em.persist(offre);
		em.persist(reservation);
	}  
    
}
