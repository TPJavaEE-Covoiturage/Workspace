package ejbs;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import Entity.TypeVehicule;

@Stateless(name="TypeVoituresDAO")
@LocalBean
public class TypeVoituresDAO {

    @PersistenceContext( unitName = "Covoiturage" )
    private EntityManager       em;
	
    public void add( TypeVehicule ville ) throws Exception {
        try {
            em.persist( ville );
        } catch ( Exception e ) {
            throw new Exception( e );
        }
    }
    
    public List<TypeVehicule> list() throws Exception {
        try {
            TypedQuery<TypeVehicule> q = em.createQuery("from TypeVehicule", TypeVehicule.class);
            return q.getResultList();
        } catch ( Exception e ) {
            throw new Exception( e );
        }
		
    }

	public void remove(TypeVehicule ville1) {
		em.remove(em.contains(ville1) ? ville1 : em.merge(ville1));
	}
}
