package ejbs;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import Entity.Ville;

@Stateless(name="VillesDAO")
@LocalBean
public class VillesDAO {

    @PersistenceContext( unitName = "Covoiturage" )
    private EntityManager       em;
	
    public void add( Ville ville ) throws Exception {
        try {
            em.persist( ville );
        } catch ( Exception e ) {
            throw new Exception( e );
        }
    }
    
    public List<Ville> list() throws Exception {
        try {
            TypedQuery<Ville> q = em.createQuery("from Ville", Ville.class);
            return q.getResultList();
        } catch ( Exception e ) {
            throw new Exception( e );
        }
		
    }

	public void remove(Ville ville1) {
		em.remove(em.contains(ville1) ? ville1 : em.merge(ville1));
	}
}
