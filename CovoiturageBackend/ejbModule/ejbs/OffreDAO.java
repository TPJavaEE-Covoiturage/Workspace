package ejbs;

import java.sql.Timestamp;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import Entity.Offre;
import Entity.User;
import Entity.Vehicule;
import Entity.Ville;

@Stateless(name="OffreDAO")
@LocalBean
public class OffreDAO {

    @PersistenceContext( unitName = "Covoiturage" )
    private EntityManager       em;
	
    public void add( Offre offre ) throws Exception {
        try {
            em.persist( offre );
        } catch ( Exception e ) {
            throw new Exception( e );
        }
    }
    
    public List<Offre> list() throws Exception {
        try {
            TypedQuery<Offre> q = em.createQuery("from Offre", Offre.class);
            return q.getResultList();
        } catch ( Exception e ) {
            throw new Exception( e );
        }
    }
    
    public List<Offre> listePourConducteur(String username) throws Exception {
        try {
            TypedQuery<Offre> q = em.createQuery(
            		"from Offre offre "
            		+ "left join fetch offre.Conducteur conducteur "
            		+ "WHERE conducteur.Name = ?1", Offre.class);
            q.setParameter(1, username);
            return q.getResultList();
        } catch ( Exception e ) {
            throw new Exception( e );
        }
    }
    
    public List<Offre> search(String Depart, String Arrivee) throws Exception {
        try {
            TypedQuery<Offre> q = em.createQuery(
            		"from Offre offre "
            		+ "left join fetch offre.Depart as depart "
            		+ "left join fetch offre.Arrivee as arrivee "
            		+ "WHERE depart.Name = ?1 "
            		+ "AND arrivee.Name = ?2 "
            		+ "ORDER BY offre.Date ", Offre.class);
            q.setParameter(1, Depart);
            q.setParameter(2, Arrivee);
            return q.getResultList();
        } catch ( Exception e ) {
            throw new Exception( e );
        }
		
    }

	public void remove(Offre offre) {
		em.remove(em.contains(offre) ? offre : em.merge(offre));
	}

	/*
	 * Add a new offer
	 */
	public void proposerTrajet(
			String depart, String arrivee, 
			int vehicule_id, int pdispo, 
			Timestamp timestamp,
			String username) {
		
		//Retreive user
		User user = em.find(User.class, username);
		if (user == null){ return;}
		
		//Retreive depart
		Ville vdepart = em.find(Ville.class, depart);
		if (vdepart == null){ return;}
		
		//Retreive arrivee
		Ville varrivee = em.find(Ville.class, arrivee);
		if (varrivee == null){ return;}
		
		//Check if arrivee != depart
		if (varrivee.getName().equals(vdepart.getName())) { return; }
		
		//Retreive vehicule
		Vehicule vehicule = em.find(Vehicule.class, vehicule_id);
		if (vehicule == null){ return;}
		
		//Check if number of places is valid
		if (pdispo < 1 || pdispo > 9){return;}
		
		//Check if the user has the good vehicle
		if (!vehicule.getOwner().getName().equals(username)){return;}
		
		//Fill new offer
		Offre offre = new Offre();
		offre.setArrivee(varrivee);
		offre.setDepart(vdepart);
		offre.setDate(timestamp);
		offre.setPlaces(pdispo);
		offre.setVehicule(vehicule);
		offre.setConducteur(user);
		em.persist(offre);
	}
}
